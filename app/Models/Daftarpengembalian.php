<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Daftarpengembalian extends Model
{
    use HasFactory;

    function pengembalian()
    {
        return $this->hasOne(Pengembalian::class, 'id', 'pengembalian_id');
    }

}
