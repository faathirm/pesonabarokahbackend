<?php

namespace App\Http\Controllers;

use App\Models\Alamat;
use App\Models\Barang;
use App\Models\Inventarismasuk;
use App\Models\Karyawan;
use App\Models\Parameter;
use App\Models\Pembayaran;
use App\Models\Pembelian;
use App\Models\Tukang;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HelperController extends Controller
{
    public function getKodeBarang()
    {
        $barangs = Barang::where('deleted_at',null)->get();
        $data = array();
        foreach($barangs as $bar){
            array_push($data,'('.$bar->kode_barang.') '.$bar->nama_barang);
        }
        return $data;
    }

    public function getDetailBarang(Request $request)
    {
        $barangs = Barang::where('kode_barang',$request->kode_barang)->first();
        return $barangs;
    }

    public function getAlamat()
    {
        $alamats = Alamat::where('deleted_at',null)->get();
        $data = array();
        foreach($alamats as $al){
            array_push($data,$al->alamat);
        }
        return $data;
    }

    public function getAlamatPembelian()
    {
        $alamats = Alamat::where('status','tersedia')->where('deleted_at',null)->get();
        $data = array();
        foreach($alamats as $al){
            array_push($data,$al->alamat);
        }
        return $data;
    }

    public function getTukang()
    {
        $tukangs = Tukang::where('deleted_at',null)->get();
        $data = array();
        foreach($tukangs as $tu){
            array_push($data,$tu->nama);
        }
        return $data;
    }

    public function getTukangHarian()
    {
        $tukangs = Tukang::where('jenis_jasa','harian')->where('deleted_at',null)->get();
        $data = array();
        foreach($tukangs as $tu){
            array_push($data,[
                'value'=>$tu->nama,
                'text'=>$tu->nama
            ]);
        }
        return $data;
    }

    public function getTukangBorongan()
    {
        $tukangs = Tukang::where('jenis_jasa','borongan')->where('deleted_at',null)->get();
        $data = array();
        foreach($tukangs as $tu){
            array_push($data,[
                'value'=>$tu->nama,
                'text'=>$tu->nama
            ]);
        }
        return $data;
    }

    public function getDetailTukang($nama)
    {
        $tukang =  Tukang::where('nama',$nama)->first();
        return "Rp. ".number_format($tukang->biaya_jasa,0, ',' , '.');
    }

    public function getInventarisMasuk()
    {
        $im = Inventarismasuk::groupBy('barang_id')->where('tipe','MATERIAL')->where('deleted_at',null)->get();
        $data = array();
        foreach($im as $i){
            array_push($data, "(".$i->barang->kode_barang.") ".$i->barang->nama_barang);
        }
        return $data;
    }

    public function getParameter(Request $request)
    {
        $param = Parameter::where('parameter',$request->parameter)->first();
        return $param;
    }

    public function getPembelian(Request $request)
    {

        $pm = DB::table('pembelians')
            ->leftJoin('pengembalians', 'pembelians.id', '=', 'pengembalians.pembelian_id')
            ->where('pengembalians.pembelian_id','=',null)
            ->where('pembelians.deleted_at','=',null)->get();
        $data = array();
        foreach($pm as $p){
            $al = Alamat::find($p->alamat_id);
            array_push($data, $p->nama_pembeli . ' (' . $al->alamat . ')');
        }
        return $data;
    }

    public function getDetailPembelian(Request $request)
    {
        $pm = Pembelian::where('nama_pembeli', $request->nama_pembeli)->with('alamatpembelian')->first();
        return $pm;
    }

    public function getPembayaranCicilan($id)
    {
        $data = array();
        $pm = Pembelian::where('nama_pembeli',$id)->first();
        $id = $pm->id;
        $pm->alamatpembelian;

        $total = Pembayaran::where('pembelian_id',$id)->where('jenis','cicilan')->sum('total_pembayaran');
        $totalDibayarkan = $pm->dp + $total;

        $sisaCicilan = $pm->harga - $pm->dp - $total;

        $jumlahCicilan = ($pm->pilihanpembayaran_id*12) - Pembayaran::where('pembelian_id',$id)->where('jenis','cicilan')->count();

        array_push($data, [
            'id'=>$pm->id,
            'nama_pemilik_rumah'=>$pm->nama_pembeli,
            'nomor_handphone'=>$pm->nomor_handphone,
            'alamat'=>$pm->alamatpembelian->alamat,
            'total_dibayarkan'=>"Rp. ".number_format($totalDibayarkan,0, ',' , '.'),
            'sisa_cicilan'=>$jumlahCicilan.'x',
            'sisa_pembayaran'=>"Rp. ".number_format($sisaCicilan,0, ',' , '.'),
        ]);

        return $data[0];
    }

    public function getKaryawan(){
        $data = array();
        $kary = Karyawan::where('deleted_at',null)->get();
        foreach($kary as $tu){
            array_push($data,[
                'value'=>$tu->nama,
                'text'=>$tu->nama
            ]);
        }
        return $data;
    }

    public function getDetailKaryawan($nama){
        $data = array();
        $kary = Karyawan::where('nama',$nama)->where('deleted_at',null)->first();
        array_push($data, [
            'id'=>$kary->id,
            'nama'=>$kary->nama,
            'jabatan'=>$kary->jabatan,
            'gaji'=>"Rp. ".number_format($kary->gaji,0, ',' , '.'),
        ]);
        return $data[0];
    }

    public function getDataPemasukanPengeluaran($tahun)
    {
        $pemasukan = [
            '0'=>0,
            '1'=>0,
            '2'=>0,
            '3'=>0,
            '4'=>0,
            '5'=>0,
            '6'=>0,
            '7'=>0,
            '8'=>0,
            '9'=>0,
            '10'=>0,
            '11'=>0,
        ];
        for($x=0;$x<(count($pemasukan));$x++){
            $tanggal = $x+1;
            $inventariskeluar = DB::table('inventariskeluars')->where('tanggal','like',$tahun.'-'.$tanggal.'-%')->where('deleted_at','=',null)->sum('total');
            $pembelian = DB::table('pembelians')->where('pilihanpembayaran_id','=','0')->where('tanggal_pembayaran','like',$tahun.'-'.$tanggal.'-%')->where('deleted_at','=',null)->sum('harga');
            $pembayaran = DB::table('pembayarans')->where('tanggal','like',$tahun.'-'.$tanggal.'-%')->where('deleted_at','=',null)->sum('total_pembayaran');
            $pemasukan[$x] = $pemasukan[$x]+$inventariskeluar+$pembelian+$pembayaran;
        }
        $data = [
            '0'=>0,
            '1'=>0,
            '2'=>0,
            '3'=>0,
            '4'=>0,
            '5'=>0,
            '6'=>0,
            '7'=>0,
            '8'=>0,
            '9'=>0,
            '10'=>0,
            '11'=>0,
        ];
        for($x=1;$x<(count($data));$x++){
            //IM
            $tanggal = $x+1;
            $inventarismasuk = DB::table('inventarismasuks')->where('tanggal','like',$tahun.'-'.$tanggal.'-%')->where('deleted_at','=',null)->sum('total');
            $inventarisoperasional = DB::table('inventarisoperasionals')->where('tanggal','like',$tahun.'-'.$tanggal.'-%')->where('deleted_at','=',null)->sum('total');
            $daftarpengembalian = DB::table('daftarpengembalians')->where('tanggal','like',$tahun.'-'.$tanggal.'-%')->where('deleted_at','=',null)->sum('jumlah');
            $gajikaryawan = DB::table('gajikaryawans')->join('karyawans','gajikaryawans.karyawan_id','=','karyawans.id')->where('gajikaryawans.tanggal','like',$tahun.'-'.$tanggal.'-%')->where('gajikaryawans.deleted_at','=',null)->sum('gaji');
            $gajitukang = DB::table('gajitukangs')->where('created_at','like',$tahun.'-'.$tanggal.'-%')->where('deleted_at','=',null)->sum('total');
            $data[$x] = $data[$x]+$inventarismasuk+$inventarisoperasional+$daftarpengembalian+$gajikaryawan+$gajitukang;
        }
        $total = array();
        array_push($total, [
            'pemasukan'=>$pemasukan,
            'pengeluaran'=>$data
        ]);
        return $total[0];
    }

}
