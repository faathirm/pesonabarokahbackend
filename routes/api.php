<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\InventarismasukController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

header('Access-Control-Allow-Origin:  *');
header('Access-Control-Allow-Methods:  POST, GET, OPTIONS, PUT, DELETE');
header('Access-Control-Allow-Headers:  Content-Type, X-Auth-Token, Origin, Authorization');

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/inventaris/getStokBarang',[\App\Http\Controllers\BarangController::class, 'getAll']);
Route::post('/inventaris/addStokBarang',[\App\Http\Controllers\BarangController::class, 'addBarang']);
Route::post('/inventaris/getStokBarang',[\App\Http\Controllers\BarangController::class, 'getBarang']);
Route::post('/inventaris/updateStokBarang',[\App\Http\Controllers\BarangController::class, 'updateBarang']);
Route::post('/inventaris/deleteStokBarang',[\App\Http\Controllers\BarangController::class, 'deleteBarang']);

Route::get('/inventaris/getInventarisMasuk',[\App\Http\Controllers\InventarismasukController::class, 'getAll']);
Route::post('/inventaris/addInventarisMasuk',[\App\Http\Controllers\InventarismasukController::class, 'addInventarismasuk']);
Route::post('/inventaris/getInventarisMasuk',[\App\Http\Controllers\InventarismasukController::class, 'getInventarismasuk']);
Route::post('/inventaris/updateInventarisMasuk',[\App\Http\Controllers\InventarismasukController::class, 'updateInventarismasuk']);
Route::post('/inventaris/deleteInventarisMasuk',[\App\Http\Controllers\InventarismasukController::class, 'deleteInventarismasuk']);

Route::get('/inventaris/getInventarisKeluar',[\App\Http\Controllers\InventariskeluarController::class, 'getAll']);
Route::post('/inventaris/addInventarisKeluar',[\App\Http\Controllers\InventariskeluarController::class, 'addInventariskeluar']);
Route::post('/inventaris/getInventarisKeluar',[\App\Http\Controllers\InventariskeluarController::class, 'getInventariskeluar']);
Route::post('/inventaris/updateInventarisKeluar',[\App\Http\Controllers\InventariskeluarController::class, 'updateInventariskeluar']);
Route::post('/inventaris/deleteInventarisKeluar',[\App\Http\Controllers\InventariskeluarController::class, 'deleteInventariskeluar']);

Route::get('/inventaris/getInventarisOperasional',[\App\Http\Controllers\InventarisoperasionalController::class, 'getAll']);
Route::post('/inventaris/addInventarisOperasional',[\App\Http\Controllers\InventarisoperasionalController::class, 'addInventarisoperasional']);
Route::post('/inventaris/getInventarisOperasional',[\App\Http\Controllers\InventarisoperasionalController::class, 'getInventarisoperasional']);
Route::post('/inventaris/updateInventarisOperasional',[\App\Http\Controllers\InventarisoperasionalController::class, 'updateInventarisoperasional']);
Route::post('/inventaris/deleteInventarisOperasional',[\App\Http\Controllers\InventarisoperasionalController::class, 'deleteInventarisoperasional']);

Route::get('/pembeli/getPembelian',[\App\Http\Controllers\PembelianController::class, 'getAll']);
Route::get('/pembeli/getKreditInfo/{id}',[\App\Http\Controllers\PembelianController::class, 'getKreditInfo']);
Route::post('/pembeli/addPembelianKavling',[\App\Http\Controllers\PembelianController::class, 'addPembelianKavling']);
Route::post('/pembeli/addPembelianRumah',[\App\Http\Controllers\PembelianController::class, 'addPembelianRumah']);
Route::post('/pembeli/getPembelian',[\App\Http\Controllers\PembelianController::class, 'getPembelian']);
Route::post('/pembeli/updatePembelianKavling',[\App\Http\Controllers\PembelianController::class, 'updatePembelianKavling']);
Route::post('/pembeli/updatePembelianRumah',[\App\Http\Controllers\PembelianController::class, 'updatePembelianRumah']);
Route::post('/pembeli/deletePembelian',[\App\Http\Controllers\PembelianController::class, 'deletePembelian']);

Route::get('/pembeli/getPengembalian',[\App\Http\Controllers\PengembalianController::class, 'getAll']);
Route::post('/pembeli/addPengembalian',[\App\Http\Controllers\PengembalianController::class, 'addPengembalian']);
Route::post('/pembeli/getPengembalian',[\App\Http\Controllers\PengembalianController::class, 'getPengembalian']);
Route::get('/pembeli/viewPengembalian/{id}',[\App\Http\Controllers\PengembalianController::class, 'viewPengembalian']);
Route::post('/pembeli/updatePengembalian',[\App\Http\Controllers\PengembalianController::class, 'updatePengembalian']);
Route::post('/pembeli/deletePengembalian',[\App\Http\Controllers\PengembalianController::class, 'deletePengembalian']);

Route::get('/pembayaran/getCicilan',[\App\Http\Controllers\PembayaranController::class, 'getAllCicilan']);
Route::get('/pembayaran/getAir',[\App\Http\Controllers\PembayaranController::class, 'getAllAir']);
Route::get('/pembayaran/getNamaPembelian/{tipe}',[\App\Http\Controllers\PembayaranController::class,'getPembelianCicilan']);
Route::post('/pembayaran/addPembayaranCicilan',[\App\Http\Controllers\PembayaranController::class, 'addPembayaranCicilan']);
Route::post('/pembayaran/addPembayaranAir',[\App\Http\Controllers\PembayaranController::class, 'addPembayaranAir']);
Route::get('/pembayaran/getPembayaran/{id}',[\App\Http\Controllers\PembayaranController::class, 'getPembayaran']);
Route::post('/pembayaran/updatePembayaranCicilan',[\App\Http\Controllers\PembayaranController::class, 'updatePembayaranCicilan']);
Route::post('/pembayaran/updatePembayaranAir',[\App\Http\Controllers\PembayaranController::class, 'updatePembayaranAir']);
Route::post('/pembayaran/deletePembayaran',[\App\Http\Controllers\PembayaranController::class, 'deletePembayaran']);

Route::get('/keluhan/getKeluhan',[\App\Http\Controllers\KeluhanController::class, 'getAll']);
Route::post('/keluhan/addKeluhan',[\App\Http\Controllers\KeluhanController::class, 'addKeluhan']);
Route::get('/keluhan/getDetailKeluhan/{id}',[\App\Http\Controllers\KeluhanController::class, 'getDetailKeluhan']);
Route::post('/keluhan/updateKeluhan',[\App\Http\Controllers\KeluhanController::class, 'updateKeluhan']);
Route::post('/keluhan/deleteKeluhan',[\App\Http\Controllers\KeluhanController::class, 'deleteKeluhan']);

Route::get('/pengguna/getPengguna',[\App\Http\Controllers\BarokahuserController::class, 'getAll']);
Route::post('/pengguna/addPengguna',[\App\Http\Controllers\BarokahuserController::class, 'addPengguna']);
Route::get('/pengguna/getDetailPengguna/{id}',[\App\Http\Controllers\BarokahuserController::class, 'getDetailPengguna']);
Route::post('/pengguna/updatePengguna',[\App\Http\Controllers\BarokahuserController::class, 'updatePengguna']);
Route::post('/pengguna/deletePengguna',[\App\Http\Controllers\BarokahuserController::class, 'deletePengguna']);

Route::get('/penggajian/getGajiKaryawan',[\App\Http\Controllers\GajikaryawanController::class, 'getAll']);
Route::post('/penggajian/addGajiKaryawan',[\App\Http\Controllers\GajikaryawanController::class, 'addGajiKaryawan']);
Route::get('/penggajian/getDetailGajiKaryawan/{id}',[\App\Http\Controllers\GajikaryawanController::class, 'getDetailGajiKaryawan']);
Route::post('/penggajian/updateGajiKaryawan',[\App\Http\Controllers\GajikaryawanController::class, 'updateGajiKaryawan']);
Route::post('/penggajian/deleteGajiKaryawan',[\App\Http\Controllers\GajikaryawanController::class, 'deleteGajiKaryawan']);

Route::get('/penggajian/getGajiTukangHarian',[\App\Http\Controllers\GajitukangController::class, 'getAllHarian']);
Route::get('/penggajian/getGajiTukangBorongan',[\App\Http\Controllers\GajitukangController::class, 'getAllBorongan']);
Route::post('/penggajian/addGajiTukangHarian',[\App\Http\Controllers\GajitukangController::class, 'addGajiTukangHarian']);
Route::get('/penggajian/getDetailGajiTukang/{id}',[\App\Http\Controllers\GajitukangController::class, 'getDetailGajiTukang']);
Route::post('/penggajian/updateGajiTukang',[\App\Http\Controllers\GajitukangController::class, 'updateGajiTukang']);
Route::post('/penggajian/deletePenggajian',[\App\Http\Controllers\GajitukangController::class, 'deletePenggajian']);

Route::post('/auth/login',[\App\Http\Controllers\BarokahuserController::class, 'login']);

//helper
Route::get('/getKodeBarang',[\App\Http\Controllers\HelperController::class, 'getKodeBarang']);
Route::post('/getDetailBarang',[\App\Http\Controllers\HelperController::class, 'getDetailBarang']);
Route::get('/getAlamat',[\App\Http\Controllers\HelperController::class, 'getAlamat']);
Route::get('/getAlamatPembelian',[\App\Http\Controllers\HelperController::class, 'getAlamatPembelian']);
Route::get('/getTukang',[\App\Http\Controllers\HelperController::class, 'getTukang']);
Route::get('/getTukangHarian',[\App\Http\Controllers\HelperController::class, 'getTukangHarian']);
Route::get('/getTukangBorongan',[\App\Http\Controllers\HelperController::class, 'getTukangBorongan']);
Route::get('/getDetailTukang/{nama}',[\App\Http\Controllers\HelperController::class, 'getDetailTukang']);
Route::get('/getInventarisMasuk',[\App\Http\Controllers\HelperController::class, 'getInventarisMasuk']);
Route::post('/getParameter',[\App\Http\Controllers\HelperController::class, 'getParameter']);
Route::get('/getPembelian',[\App\Http\Controllers\HelperController::class, 'getPembelian']);
Route::post('/getDetailPembelian',[\App\Http\Controllers\HelperController::class, 'getDetailPembelian']);
Route::get('/getPembayaranCicilan/{id}',[\App\Http\Controllers\HelperController::class, 'getPembayaranCicilan']);
Route::get('/getKaryawan',[\App\Http\Controllers\HelperController::class, 'getKaryawan']);
Route::get('/getDetailKaryawan/{nama}',[\App\Http\Controllers\HelperController::class, 'getDetailKaryawan']);
Route::get('/getDataPemasukanPengeluaran/{tahun}',[\App\Http\Controllers\HelperController::class, 'getDataPemasukanPengeluaran']);
Route::get('/getDataProfitLoss/{tahun}',[\App\Http\Controllers\HelperController::class, 'getDataProfitLoss']);

//print
Route::get('/print/pembayaran/{id}',[\App\Http\Controllers\PrintController::class, 'pembayaran']);
Route::get('/print/pengembalian/{id}',[\App\Http\Controllers\PrintController::class, 'pengembalian']);
Route::get('/print/pembelian/{id}',[\App\Http\Controllers\PrintController::class, 'pembelian']);
Route::post('/print/pembelian_view/',[\App\Http\Controllers\PrintController::class, 'pembelian_view']);
Route::get('/print/penggajian/karyawan/{id}',[\App\Http\Controllers\PrintController::class, 'penggajian_karyawan']);
Route::get('/print/penggajian/tukang/{id}',[\App\Http\Controllers\PrintController::class, 'penggajian_tukang']);

//export
Route::get('/export/dashboard',[\App\Http\Controllers\ExportController::class, 'dashboard']);
Route::get('/export/inventarismasuk',[\App\Http\Controllers\ExportController::class, 'inventarismasuk']);

Route::get('/clear-cache', function() {
    $exitCode = Artisan::call('cache:clear');
    // return what you want
});
