<?php

namespace App\Http\Controllers;

use App\Models\Keluhan;
use Carbon\Carbon;
use Illuminate\Http\Request;

class KeluhanController extends Controller
{
    public function getAll()
    {
        $kel = Keluhan::where('deleted_at',null)->with('pembelian.alamatpembelian')->get();

        return $kel;
    }

    public function addKeluhan(Request $request)
    {
        $kel = new Keluhan();
        $kel->tanggal = $request->tanggal;
        $kel->pembelian_id = $request->pembelian_id;
        $kel->keluhan = $request->keluhan;
        $kel->kebutuhan = $request->kebutuhan;
        $kel->status = $request->status;
        $kel->save();

        return $kel;
    }

    public function getDetailKeluhan(Request $request)
    {
        $kel = Keluhan::find($request->id);
        $kel->pembelian->alamatpembelian;
        return $kel;
    }

    public function updateKeluhan(Request $request)
    {
        $kel = Keluhan::find($request->id);
        $kel->tanggal = $request->tanggal;
        $kel->pembelian_id = $request->pembelian_id;
        $kel->keluhan = $request->keluhan;
        $kel->kebutuhan = $request->kebutuhan;
        $kel->status = $request->status;
        $kel->save();

        return $kel;
    }

    public function deleteKeluhan(Request $request)
    {
        $kel = Keluhan::find($request->id);
        $kel->deleted_at = Carbon::now();
        $kel->save();

        return $kel;
    }
}
