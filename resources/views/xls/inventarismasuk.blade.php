<html>
<head>
</head>
<body>
<table style="word-wrap:break-word">
    <thead>
    <tr>
        <th><strong>No</strong></th>
        <th><strong>Tanggal</strong></th>
        <th><strong>Supplier</strong></th>
        <th><strong>Nama Barang</strong></th>
        <th><strong>Satuan Barang</strong></th>
        <th><strong>Kuantitas</strong></th>
        <th><strong>Harga Satuan</strong></th>
        <th><strong>Total Pembelian</strong></th>
    </tr>
    </thead>
    <tbody>
    @php $total = 0; @endphp
    @for($x=0;$x<count($data);$x++)
        <tr>
            <td>{{$x+1}}</td>
            <td>{{$data[$x]['tanggal']}}</td>
            <td>{{$data[$x]['supplier']}}</td>
            <td>{{$data[$x]['nama_barang']}}</td>
            <td>{{$data[$x]['satuan']}}</td>
            <td>{{$data[$x]['kuantitas']}}</td>
            <td>{{$data[$x]['harga_satuan']}}</td>
            <td>{{$data[$x]['total']}}</td>
{{--            <td>{{$data['pemasukan'][$x]}}</td>--}}
{{--            <td>{{$data['pengeluaran'][$x]}}</td>--}}
            @php
                $total = $data[$x]['total'] + $total;
            @endphp
        </tr>
    @endfor
    <tr>
        <td colspan="7"><strong>Total</strong></td>
        <td>{{$total}}</td>
    </tr>
    </tbody>
</table>
</body>
</html>