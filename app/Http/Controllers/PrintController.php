<?php

namespace App\Http\Controllers;

use App\Models\Daftarpengembalian;
use App\Models\Gajikaryawan;
use App\Models\Gajitukang;
use App\Models\Pembayaran;
use App\Models\Pembelian;
use App\Models\Pengembalian;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class PrintController extends Controller
{
    public function pembayaran($id)
    {
        $pembayaran = Pembayaran::where('id',$id)->with('pembelian.alamatpembelian')->first();
        $pdf = App::make('dompdf.wrapper');
        $pdf->loadView('pdf.pembayaran', ['pembayaran'=>$pembayaran]);
        return $pdf->stream(strtoupper("PEMBAYARAN ".$pembayaran->jenis." - ".$pembayaran->bulan." - ".$pembayaran->pembelian->nama_pembeli));
    }

    public function pengembalian($id)
    {
        $pengembalian = Daftarpengembalian::where('id',$id)->with('pengembalian.pembelian.alamatpembelian')->first();
        $pdf = App::make('dompdf.wrapper');
        $pdf->loadView('pdf.pengembalian', ['pengembalian'=>$pengembalian]);
        return $pdf->stream(strtoupper("PENGEMBALIAN - ".\Carbon\Carbon::createFromFormat('Y-m-d', $pengembalian->tanggal)->isoFormat('D MMMM Y')." - ".$pengembalian->pengembalian->pembelian->nama_pembeli));
    }

    public function penggajian_karyawan($id)
    {
        $penggajian = Gajikaryawan::where('id',$id)->with('karyawan')->first();
        $pdf = App::make('dompdf.wrapper');
        $pdf->loadView('pdf.penggajian', ['penggajian'=>$penggajian]);
        return $pdf->stream(strtoupper("PENGGAJIAN - ".\Carbon\Carbon::createFromFormat('Y-m-d', $penggajian->tanggal)->isoFormat('D MMMM Y')." - ".$penggajian->karyawan->nama));
    }

    public function penggajian_tukang($id)
    {
        $penggajian = Gajitukang::where('id',$id)->with('tukang')->first();
        $pdf = App::make('dompdf.wrapper');
        $pdf->loadView('pdf.penggajian_tukang', ['penggajian'=>$penggajian]);
        return $pdf->stream(strtoupper("PENGGAJIAN - ".$penggajian->created_at->isoFormat('D MMMM Y')." - ".$penggajian->tukang->nama));
    }

    public function pembelian($id)
    {
        $pembelian = Pembelian::where('id',$id)->with('alamatpembelian')->first();
        $pdf = App::make('dompdf.wrapper');
        $pdf->loadView('pdf.pembelian', ['pembelian'=>$pembelian]);
        return $pdf->stream(strtoupper("PEMBELIAN - ".$pembelian->alamatpembelian->alamat." - ".$pembelian->nama_pembeli));
//        if($pembelian->pilihanpembayaran_id == 0){
//            $pdf->loadView('pdf.pembelian_rumah_cash', ['pembelian'=>$pembelian]);
//            return $pdf->stream(strtoupper("PEMBELIAN RUMAH CASH - ".$pembelian->alamatpembelian->alamat." - ".$pembelian->nama_pembeli));
//        }else{
//            $pdf->loadView('pdf.pembelian_rumah_kredit', ['pembelian'=>$pembelian]);
//            return $pdf->stream(strtoupper("PEMBELIAN RUMAH KREDIT - ".$pembelian->alamatpembelian->alamat." - ".$pembelian->nama_pembeli));
//        }
    }

    public function pembelian_view(Request $request)
    {
        $pembelian = new \stdClass();
        $pembelian->alamatpembelian = new \stdClass();
        $pembelian->nama_pembeli = $request->nama_pembeli;
        $pembelian->nik = $request->nik;
        $pembelian->alamat = $request->alamat;
        $pembelian->nomor_handphone = $request->nomor_handphone;
        $pembelian->tanggal_pembayaran = $request->tanggal_pembayaran;
        $pembelian->luas_tanah = $request->luas_tanah;
        $pembelian->luas_bangunan = $request->luas_bangunan;
        $pembelian->harga_meter = preg_replace("/[^0-9]/", "",$request->harga_meter);
        $pembelian->pilihanpembayaran_id = $request->pilihanpembayaran_id;
        $pembelian->pilihanpembayaran_id = $request->pilihanpembayaran_id;
        $pembelian->dp = preg_replace("/[^0-9]/", "",$request->dp);;
        $pembelian->harga = preg_replace("/[^0-9]/", "",$request->harga);;
        $pembelian->alamatpembelian->alamat = $request->alamatpembelian;

        $pdf = App::make('dompdf.wrapper');
        if($pembelian->pilihanpembayaran_id == 0){
            $pdf->loadView('pdf.pembelian_rumah_cash', ['pembelian'=>$pembelian]);
        }else{
            $pdf->loadView('pdf.pembelian_rumah_kredit', ['pembelian'=>$pembelian]);
        }
        return $pdf->stream();
    }
}

