<?php

namespace App\Http\Controllers;

use App\Models\Barokahuser;
use App\Models\Barokahuseracl;
use App\Models\Keluhan;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class BarokahuserController extends Controller
{
    public function getAll()
    {
        $peng = Barokahuser::where('deleted_at',null)->with('barokahuseracl')->get();

        return $peng;
    }

    public function addPengguna(Request $request)
    {
        $peng = new Barokahuser();
        $peng->username = $request->username;
        $peng->nama = $request->nama;
        $peng->password = Hash::make($request->password);
        $peng->save();
        foreach($request->acl as $acl){
            $pengacl = new Barokahuseracl();
            $pengacl->user_id = $peng->id;
            $pengacl->access = $acl;
            $pengacl->save();
        }
        return $peng;
    }

    public function getDetailPengguna($id)
    {
        $peng = Barokahuser::where('id',$id)->with('barokahuseracl')->first();
        return $peng;
    }

    public function updatePengguna(Request $request)
    {
        $peng = Barokahuser::find($request->id);
        $peng->username = $request->username;
        $peng->nama = $request->nama;
        if($request->password){
            $peng->password = Hash::make($request->password);
        }
        $peng->save();
        $delete = Barokahuseracl::where('user_id',$request->id)->delete();
        foreach($request->acl as $acl){
            $pengacl = new Barokahuseracl();
            $pengacl->user_id = $peng->id;
            $pengacl->access = $acl;
            $pengacl->save();
        }
        return $peng;
    }

    public function deletePengguna(Request $request)
    {
        $peng = Barokahuser::find($request->id);
        $peng->deleted_at = Carbon::now();
        $peng->save();

        return $peng;
    }

    public function login(Request $request)
    {
        $user = Barokahuser::where('username',$request->username)->with('barokahuseracl')->first();
        if($user) {
            if (Hash::check($request->password, $user->password)) {
                return $user;
            }else{
                return abort(401, 'Username/password salah!');
            }
        }else{
            return abort(401, 'Username/password salah!');
        }
    }
}
