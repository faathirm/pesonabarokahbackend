<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Inventarismasuk extends Model
{
    use HasFactory;

    public function Barang()
    {
        return $this->hasOne(Barang::class, 'id', 'barang_id');
    }

}
