<?php

namespace App\Http\Controllers;

use App\Models\Inventarismasuk;
use Illuminate\Http\Request;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\FromView;
use Excel;

class ExportController extends Controller
{
    public function dashboard()
    {
        return Excel::download(new DashboardExport, 'dashboard.xlsx');
    }

    public function inventarismasuk()
    {
        return Excel::download(new inventarismasukExport, 'inventarismasuk.xlsx');
    }
}

class DashboardExport implements FromView
{
    public function view(): View
    {
        $tahun = date("Y");
        $pemasukan = [
            '0'=>0,
            '1'=>0,
            '2'=>0,
            '3'=>0,
            '4'=>0,
            '5'=>0,
            '6'=>0,
            '7'=>0,
            '8'=>0,
            '9'=>0,
            '10'=>0,
            '11'=>0,
        ];
        for($x=0;$x<(count($pemasukan));$x++){
            $tanggal = $x+1;
            $inventariskeluar = DB::table('inventariskeluars')->where('tanggal','like',$tahun.'-'.$tanggal.'-%')->where('deleted_at','=',null)->sum('total');
            $pembelian = DB::table('pembelians')->where('pilihanpembayaran_id','=','0')->where('tanggal_pembayaran','like',$tahun.'-'.$tanggal.'-%')->where('deleted_at','=',null)->sum('harga');
            $pembayaran = DB::table('pembayarans')->where('tanggal','like',$tahun.'-'.$tanggal.'-%')->where('deleted_at','=',null)->sum('total_pembayaran');
            $pemasukan[$x] = $pemasukan[$x]+$inventariskeluar+$pembelian+$pembayaran;
        }
        $data = [
            '0'=>0,
            '1'=>0,
            '2'=>0,
            '3'=>0,
            '4'=>0,
            '5'=>0,
            '6'=>0,
            '7'=>0,
            '8'=>0,
            '9'=>0,
            '10'=>0,
            '11'=>0,
        ];
        for($x=1;$x<(count($data));$x++){
            //IM
            $tanggal = $x+1;
            $inventarismasuk = DB::table('inventarismasuks')->where('tanggal','like',$tahun.'-'.$tanggal.'-%')->where('deleted_at','=',null)->sum('total');
            $inventarisoperasional = DB::table('inventarisoperasionals')->where('tanggal','like',$tahun.'-'.$tanggal.'-%')->where('deleted_at','=',null)->sum('total');
            $daftarpengembalian = DB::table('daftarpengembalians')->where('tanggal','like',$tahun.'-'.$tanggal.'-%')->where('deleted_at','=',null)->sum('jumlah');
            $gajikaryawan = DB::table('gajikaryawans')->join('karyawans','gajikaryawans.karyawan_id','=','karyawans.id')->where('gajikaryawans.tanggal','like',$tahun.'-'.$tanggal.'-%')->where('gajikaryawans.deleted_at','=',null)->sum('gaji');
            $gajitukang = DB::table('gajitukangs')->where('created_at','like',$tahun.'-'.$tanggal.'-%')->where('deleted_at','=',null)->sum('total');
            $data[$x] = $data[$x]+$inventarismasuk+$inventarisoperasional+$daftarpengembalian+$gajikaryawan+$gajitukang;
        }
        $total = array();
        array_push($total, [
            'pemasukan'=>$pemasukan,
            'pengeluaran'=>$data
        ]);
        return view('xls.dashboard', [
            'data' => $total[0]
        ]);
    }
}

class inventarismasukExport implements  fromView
{
    public function view(): View
    {
        $im = Inventarismasuk::where('deleted_at',null)->get();
        $data = array();
        foreach($im as $i){
            array_push($data,[
                'id'=>$i->id,
                'supplier'=>$i->supplier,
                'tanggal'=>$i->tanggal,
                'nama_barang'=>$i->barang->nama_barang,
                'satuan'=>$i->barang->satuan,
                'kuantitas'=>$i->kuantitas,
                'harga_satuan'=>$i->harga_satuan,
                'total'=>$i->total,
                'file'=>$i->file
            ]);
        }
        return view('xls.inventarismasuk',[
            'data'=>$data
        ]);
    }
}