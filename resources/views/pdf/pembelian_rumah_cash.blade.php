<html>
<head>
    {{--    <meta charset="UTF-8" />--}}
    {{--    <meta name="viewport" content="width=device-width, initial-scale=1.0" />--}}
    {{--    <link href="{{ asset('css/app.css') }}" rel="stylesheet">--}}
    <title>{{strtoupper("PEMBELIAN RUMAH CASH - ".$pembelian->alamatpembelian->alamat." - ".$pembelian->nama_pembeli)}}</title>
</head>
<style>
    @font-face {
        font-family: 'Roboto';
        font-weight: normal;
        font-style: normal;
        font-variant: normal;
        src: url("http://fonts.googleapis.com/css?family=Roboto");
    }
    body{
        font-family: 'Roboto', sans-serif;
    }
    .with-border{
        border: 1px solid black;
    }
    .text-center{
        text-align: center;
    }
    .p-1{
        padding: 3px;
    }
    .font-small{
        font-size: 14px;
    }
</style>
<body class="font-small">
<h1 style="text-align: center; border-bottom: 1px solid black; padding-bottom: 5px;">PEMBELIAN RUMAH CASH</h1><br>
<table style="width: 100%;">
    <tr>
        <td style="width: 35%;">Nama</td>
        <td style="width: 65%; font-weight: bold;">{{$pembelian->nama_pembeli}}</td>
    </tr>
    <tr>
        <td style="width: 35%;">NIK</td>
        <td style="width: 65%; font-weight: bold;">{{$pembelian->nik}}</td>
    </tr>
    <tr>
        <td style="width: 35%;">Alamat</td>
        <td style="width: 65%; font-weight: bold;">{{$pembelian->alamat}}</td>
    </tr>
    <tr>
        <td style="width: 35%;">Nomor Handphone</td>
        <td style="width: 65%; font-weight: bold;">{{$pembelian->nomor_handphone}}</td>
    </tr>
    <tr>
        <td style="width: 35%;">Tanggal</td>
        <td style="width: 65%; font-weight: bold;">{{\Carbon\Carbon::createFromFormat('Y-m-d', $pembelian->tanggal_pembayaran)->isoFormat('D MMMM Y')}}</td>
    </tr>
</table>
<table class="with-border" style="width: 100%; margin-top: 25px; border-collapse: collapse">
    <tr>
        <td class="with-border" style="padding: 5px; width: 35%">Luas Tanah</td>
        <td class="with-border" style="padding: 5px; width: 65%; font-weight: bold;">{{$pembelian->luas_tanah}} m2</td>
    </tr>
    <tr>
        <td class="with-border" style="padding: 5px; width: 35%">Luas Bangunan</td>
        <td class="with-border" style="padding: 5px; width: 65%; font-weight: bold;">{{$pembelian->luas_bangunan}} m2</td>
    </tr>
    <tr>
        <td class="with-border" style="padding: 5px; width: 35%">Harga Rumah Per Meter</td>
        <td class="with-border" style="padding: 5px; width: 65%; font-weight: bold;">{{"Rp. ".number_format($pembelian->harga_meter,0, ',' , '.')}}</td>--}}</td>
    </tr>
    <tr>
        <td class="with-border" style="padding: 5px; width: 35%">Total Harga Rumah</td>
        <td class="with-border" style="padding: 5px; width: 65%; font-weight: bold;">{{"Rp. ".number_format($pembelian->harga,0, ',' , '.')}}</td>--}}</td>
    </tr>
</table>
</body>
</html>
