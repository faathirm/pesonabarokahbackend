<?php

namespace App\Http\Controllers;

use App\Models\Pembayaran;
use App\Models\Pembelian;
use Carbon\Carbon;
use Illuminate\Http\Request;

class PembayaranController extends Controller
{
    public function getAllCicilan()
    {
        $pem = Pembayaran::where('jenis','cicilan')->where('deleted_at',null)->with('pembelian.alamatpembelian')->get();
        return $pem;
    }

    public function getAllAir()
    {
        $pem = Pembayaran::where('jenis','air')->where('deleted_at',null)->with('pembelian.alamatpembelian')->get();
        return $pem;
    }

    public function getPembelianCicilan($tipe){
        if($tipe == "cicilan"){
            $pb = Pembelian::where('pilihanpembayaran_id','!=','0')->where('deleted_at',null)->with('alamatpembelian')->get();
        }else{
            $pb = Pembelian::where('deleted_at',null)->with('alamatpembelian')->get();
        }
        $data = array();
        foreach($pb as $p){
            array_push($data, $p->nama_pembeli.' ('.$p->alamatpembelian->alamat.')');
        }
        return $data;
    }

    public function addPembayaranCicilan(Request $request)
    {
        $pem = new Pembayaran();
        $pem->pembelian_id = $request->id_pembeli;
        $pem->tanggal = $request->tanggal;
        $pem->jenis = 'cicilan';
        $pem->bulan = $request->bulan;
        $pem->total_pembayaran = preg_replace("/[^0-9]/", "",$request->total_pembayaran);
        $pem->save();

        return $pem;
    }

    public function addPembayaranAir(Request $request)
    {
        $pem = new Pembayaran();
        $pem->pembelian_id = $request->id_pembeli;
        $pem->tanggal = $request->tanggal;
        $pem->jenis = 'air';
        $pem->bulan = $request->bulan;
        $pem->total_pembayaran = preg_replace("/[^0-9]/", "",$request->total_pembayaran);
        $pem->save();

        return $pem;
    }

    public function getPembayaran($id)
    {
        $pem = Pembayaran::find($id);
        $pem->pembelian->alamatpembelian;
        return $pem;
    }

    public function updatePembayaranCicilan(Request $request)
    {
        $pem = Pembayaran::find($request->id);
        $pem->pembelian_id = $request->id_pembeli;
        $pem->tanggal = $request->tanggal;
        $pem->jenis = 'cicilan';
        $pem->bulan = $request->bulan;
        $pem->total_pembayaran = preg_replace("/[^0-9]/", "",$request->total_pembayaran);
        $pem->save();

        return $pem;
    }

    public function updatePembayaranAir(Request $request)
    {
        $pem = Pembayaran::find($request->id);
        $pem->pembelian_id = $request->id_pembeli;
        $pem->tanggal = $request->tanggal;
        $pem->jenis = 'air';
        $pem->bulan = $request->bulan;
        $pem->total_pembayaran = preg_replace("/[^0-9]/", "",$request->total_pembayaran);
        $pem->save();

        return $pem;
    }

    public function deletePembayaran(Request $request)
    {
        $pem = Pembayaran::find($request->id);
        $pem->deleted_at = Carbon::now();
        $pem->save();

        return $pem;
    }
}
