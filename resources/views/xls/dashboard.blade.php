<html>
<head>
</head>
<body>
    <table style="word-wrap:break-word">
        <thead>
            <tr>
                <th><strong>No</strong></th>
                <th><strong>Bulan</strong></th>
                <th><strong>Pemasukan</strong></th>
                <th><strong>Pengeluaran</strong></th>
            </tr>
        </thead>
        <tbody>
        @php
            $bulan = [
            '0'=>"Januari",
            '1'=>"Februari",
            '2'=>"Maret",
            '3'=>"April",
            '4'=>"Mei",
            '5'=>"Juni",
            '6'=>"Juli",
            '7'=>"Agustus",
            '8'=>"September",
            '9'=>"Oktober",
            '10'=>"November",
            '11'=>"Desember",
        ];
        $pemasukan = 0;
        $pengeluaran = 0;
        @endphp
        @for($x=0;$x<count($data['pemasukan']);$x++)
            <tr>
                <td>{{$x+1}}</td>
                <td>{{$bulan[$x]}}</td>
                <td>{{$data['pemasukan'][$x]}}</td>
                <td>{{$data['pengeluaran'][$x]}}</td>
                @php
                    $pemasukan = $data['pemasukan'][$x] + $pemasukan;
                    $pengeluaran = $data['pengeluaran'][$x] + $pengeluaran;
                @endphp
            </tr>
            @endfor
        <tr>
            <td colspan="2"><strong>Total</strong></td>
            <td>{{$pemasukan}}</td>
            <td>{{$pengeluaran}}</td>
        </tr>
        </tbody>
    </table>
</body>
</html>