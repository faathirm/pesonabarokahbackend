<?php

namespace App\Http\Controllers;

use App\Models\Inventarismasuk;
use App\Models\Inventarisoperasional;
use Carbon\Carbon;
use Illuminate\Http\Request;

class InventarisoperasionalController extends Controller
{
    public function getAll()
    {
        $im = Inventarisoperasional::where('deleted_at',null)->get();
        return $im;
    }

    public function addInventarisoperasional(Request $request)
    {

        $im = new Inventarisoperasional();
        $im->kode_barang = $request->kode_barang;
        $im->nama_barang = strtoupper($request->nama_barang);
        $im->satuan = strtoupper($request->satuan);
        $im->tanggal = $request->tanggal;
        $im->harga_satuan = preg_replace("/[^0-9]/", "",$request->harga_satuan);
        $im->kuantitas = $request->kuantitas;
        $im->total = preg_replace("/[^0-9]/", "",$request->total);
        $im->save();

        return $im;
    }

    public function getInventarisoperasional(Request $request)
    {
        $im = Inventarisoperasional::find($request->id);
        $im->Barang;
        return $im;
    }

    public function updateInventarisoperasional(Request $request)
    {
        $im = Inventarisoperasional::find($request->id);

//        $im->kode_barang = $request->kode_barang;
        $im->nama_barang = strtoupper($request->nama_barang);
        $im->satuan = strtoupper($request->satuan);
        $im->tanggal = $request->tanggal;
        $im->harga_satuan = preg_replace("/[^0-9]/", "",$request->harga_satuan);
        $im->kuantitas = $request->kuantitas;
        $im->total = preg_replace("/[^0-9]/", "",$request->total);
        $im->save();

        return $im;
    }

    public function deleteInventarisoperasional(Request $request)
    {
        $im = Inventarisoperasional::find($request->id);
        $im->deleted_at = Carbon::now();
        $im->save();

        return $im;
    }
}
