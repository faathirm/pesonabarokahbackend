<?php

namespace App\Http\Controllers;

use App\Models\Alamat;
use App\Models\Barang;
use App\Models\Parameter;
use App\Models\Pembelian;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class PembelianController extends Controller
{
    public function getAll()
    {
        $pembelians = Pembelian::with('alamatpembelian')->where('deleted_at',null)->get();
        return $pembelians;
    }

    public function getKreditInfo($id)
    {
        $pem = Pembelian::find($id);
        $parameters = Parameter::where('deleted_at',null)->where('parameter','harga_tanah')->first();

        $hargaCash = ($pem->luas_bangunan*$parameters->value)+($pem->luas_tanah* $pem->harga_meter);
        $nilaiKredit = $hargaCash-$pem->dp;
        $jumlahCicilan = ($nilaiKredit/($pem->pilihanpembayaran_id*12))+($nilaiKredit*(0.08/12));
        $jumlahKreditPokok = $jumlahCicilan*($pem->pilihanpembayaran_id*12);

        $data = array(
            'harga_cash'=>$hargaCash,
            'nilai_kredit'=>$nilaiKredit,
            'jumlah_cicilan'=>$jumlahCicilan,
            'jumlah_kredit_pokok'=>$jumlahKreditPokok
        );
        return $data;
    }

    public function addPembelianKavling(Request $request)
    {
        $pb = new Pembelian();
        $pb->pilihanpembayaran_id = 0;
        $pb->jenis = "kavling";
        $pb->luas_tanah = $request->luas_tanah;
        $pb->luas_bangunan = 0;
        $pb->harga_meter = 0;
        $pb->nama_pembeli = $request->nama_pembeli;
        $pb->nik = $request->nik;
        $pb->alamat = $request->alamat;
        $pb->nomor_handphone = $request->nomor_handphone;
        $pb->harga = preg_replace("/[^0-9]/", "",$request->harga);
        $pb->dp = 0;
        $pb->tanggal_pembayaran = $request->tanggal_pembayaran;
        $pb->tanggal_akad = $request->tanggal_akad;
        $pb->jenis_pembayaran = $request->jenis_pembayaran;
        $pb->no_kwitansi = $request->no_kwitansi;

        //ALAMAT
        $al = Alamat::where('alamat',$request->alamatpembelian)->first();
        $pb->alamat_id = $al->id;

        //FILE
        if($request->file('ktp')){
            $ktp = $request->file('ktp');
            $ktp_destination = 'doc/ktp/';
            $ktp_name = date('YmdHis') . "." . $ktp->getClientOriginalExtension();
            $ktp->move($ktp_destination, $ktp_name);
            $pb->ktp = $ktp_name;
        }

        if($request->file('npwp')){
            $npwp = $request->file('npwp');
            $npwp_destination = 'doc/npwp/';
            $npwp_name = date('YmdHis') . "." . $npwp->getClientOriginalExtension();
            $npwp->move($npwp_destination, $npwp_name);
            $pb->npwp = $npwp_name;
        }

        if($request->file('surat_nikah')){
            $sn = $request->file('surat_nikah');
            $sn_destination = 'doc/surat_nikah/';
            $sn_name = date('YmdHis') . "." . $sn->getClientOriginalExtension();
            $sn->move($sn_destination, $sn_name);
            $pb->surat_nikah = $sn_name;
        }

        if($request->file('surat_akta')){
            $sa = $request->file('surat_akta');
            $sa_destination = 'doc/surat_akta/';
            $sa_name = date('YmdHis') . "." . $sa->getClientOriginalExtension();
            $sa->move($sa_destination, $sa_name);
            $pb->surat_akta = $sa_name;
        }

        $pb->save();

        $al = Alamat::find($al->id);
        $al->status = "Tidak Tersedia";
        $al->save();

        return $pb;
    }

    public function addPembelianRumah(Request $request)
    {
        $pb = new Pembelian();
        if($request->pilihanpembayaran_id){
            $pb->pilihanpembayaran_id = $request->pilihanpembayaran_id;
        }else{
            $pb->pilihanpembayaran_id = 0;
        }

        $pb->jenis = "bangunan";
        $pb->luas_tanah = $request->luas_tanah;
        $pb->luas_bangunan = $request->luas_bangunan;
        $pb->harga_meter = preg_replace("/[^0-9]/", "",$request->harga_meter);;
        $pb->nama_pembeli = $request->nama_pembeli;
        $pb->nik = $request->nik;
        $pb->alamat = $request->alamat;
        $pb->nomor_handphone = $request->nomor_handphone;
        $pb->harga = preg_replace("/[^0-9]/", "",$request->harga);

        if($request->dp){
            $pb->dp = preg_replace("/[^0-9]/", "",$request->dp);
        }else{
            $pb->dp = 0;
        }
        $pb->tanggal_pembayaran = $request->tanggal_pembayaran;
        $pb->tanggal_akad = $request->tanggal_akad;
        $pb->jenis_pembayaran = $request->jenis_pembayaran;
        $pb->no_kwitansi = $request->no_kwitansi;

        //ALAMAT
        $al = Alamat::where('alamat',$request->alamatpembelian)->first();
        $pb->alamat_id = $al->id;

        //FILE
        if($request->file('ktp')){
            $ktp = $request->file('ktp');
            $ktp_destination = 'doc/ktp/';
            $ktp_name = date('YmdHis') . "." . $ktp->getClientOriginalExtension();
            $ktp->move($ktp_destination, $ktp_name);
            $pb->ktp = $ktp_name;
        }

        if($request->file('npwp')){
            $npwp = $request->file('npwp');
            $npwp_destination = 'doc/npwp/';
            $npwp_name = date('YmdHis') . "." . $npwp->getClientOriginalExtension();
            $npwp->move($npwp_destination, $npwp_name);
            $pb->npwp = $npwp_name;
        }

        if($request->file('surat_nikah')){
            $sn = $request->file('surat_nikah');
            $sn_destination = 'doc/surat_nikah/';
            $sn_name = date('YmdHis') . "." . $sn->getClientOriginalExtension();
            $sn->move($sn_destination, $sn_name);
            $pb->surat_nikah = $sn_name;
        }

        if($request->file('surat_akta')){
            $sa = $request->file('surat_akta');
            $sa_destination = 'doc/surat_akta/';
            $sa_name = date('YmdHis') . "." . $sa->getClientOriginalExtension();
            $sa->move($sa_destination, $sa_name);
            $pb->surat_akta = $sa_name;
        }

        $pb->save();

        $al = Alamat::find($al->id);
        $al->status = "Tidak Tersedia";
        $al->save();

        return $pb;
    }

    public function getPembelian(Request $request)
    {
        $pb = Pembelian::where('id',$request->id)->with('alamatpembelian')->first();
        return $pb;
    }

    public function updatePembelianKavling(Request $request)
    {
        $pb = Pembelian::find($request->id);
        $pb->pilihanpembayaran_id = 0;
        $pb->jenis = "kavling";
        $pb->luas_tanah = $request->luas_tanah;
        $pb->luas_bangunan = 0;
        $pb->harga_meter = 0;
        $pb->nama_pembeli = $request->nama_pembeli;
        $pb->nik = $request->nik;
        $pb->alamat = $request->alamat;
        $pb->nomor_handphone = $request->nomor_handphone;
        $pb->harga = preg_replace("/[^0-9]/", "",$request->harga);
        $pb->dp = 0;
        $pb->tanggal_pembayaran = $request->tanggal_pembayaran;
        $pb->tanggal_akad = $request->tanggal_akad;
        $pb->jenis_pembayaran = $request->jenis_pembayaran;
        $pb->no_kwitansi = $request->no_kwitansi;

        //ALAMAT
        $al = Alamat::where('alamat',$request->alamatpembelian)->first();
        $pb->alamat_id = $al->id;

        //FILE
        if($request->file('ktp')){
            $ktp = $request->file('ktp');
            $ktp_destination = 'doc/ktp/';
            $ktp_name = date('YmdHis') . "." . $ktp->getClientOriginalExtension();
            $ktp->move($ktp_destination, $ktp_name);
            $pb->ktp = $ktp_name;
        }

        if($request->file('npwp')){
            $npwp = $request->file('npwp');
            $npwp_destination = 'doc/npwp/';
            $npwp_name = date('YmdHis') . "." . $npwp->getClientOriginalExtension();
            $npwp->move($npwp_destination, $npwp_name);
            $pb->npwp = $npwp_name;
        }

        if($request->file('surat_nikah')){
            $sn = $request->file('surat_nikah');
            $sn_destination = 'doc/surat_nikah/';
            $sn_name = date('YmdHis') . "." . $sn->getClientOriginalExtension();
            $sn->move($sn_destination, $sn_name);
            $pb->surat_nikah = $sn_name;
        }

        if($request->file('surat_akta')){
            $sa = $request->file('surat_akta');
            $sa_destination = 'doc/surat_akta/';
            $sa_name = date('YmdHis') . "." . $sa->getClientOriginalExtension();
            $sa->move($sa_destination, $sa_name);
            $pb->surat_akta = $sa_name;
        }

        $pb->save();

        $al = Alamat::find($al->id);
        $al->status = "Tidak Tersedia";
        $al->save();

        return $pb;
    }

    public function updatePembelianRumah(Request $request)
    {
        $pb = Pembelian::find($request->id);
        if($request->pilihanpembayaran_id){
            $pb->pilihanpembayaran_id = $request->pilihanpembayaran_id;
        }else{
            $pb->pilihanpembayaran_id = 0;
        }
        $pb->jenis = "bangunan";
        $pb->luas_tanah = $request->luas_tanah;
        $pb->luas_bangunan = $request->luas_bangunan;
        $pb->harga_meter = preg_replace("/[^0-9]/", "",$request->harga_meter);;
        $pb->nama_pembeli = $request->nama_pembeli;
        $pb->nik = $request->nik;
        $pb->alamat = $request->alamat;
        $pb->nomor_handphone = $request->nomor_handphone;
        $pb->harga = preg_replace("/[^0-9]/", "",$request->harga);
        if($request->dp){
            $pb->dp = preg_replace("/[^0-9]/", "",$request->dp);
        }else{
            $pb->dp = 0;
        }
        $pb->tanggal_pembayaran = $request->tanggal_pembayaran;
        $pb->tanggal_akad = $request->tanggal_akad;
        $pb->jenis_pembayaran = $request->jenis_pembayaran;
        $pb->no_kwitansi = $request->no_kwitansi;

        //ALAMAT
        $al = Alamat::where('alamat',$request->alamatpembelian)->first();
        $pb->alamat_id = $al->id;

        //FILE
        if($request->file('ktp')){
            $ktp = $request->file('ktp');
            $ktp_destination = 'doc/ktp/';
            $ktp_name = date('YmdHis') . "." . $ktp->getClientOriginalExtension();
            $ktp->move($ktp_destination, $ktp_name);
            $pb->ktp = $ktp_name;
        }

        if($request->file('npwp')){
            $npwp = $request->file('npwp');
            $npwp_destination = 'doc/npwp/';
            $npwp_name = date('YmdHis') . "." . $npwp->getClientOriginalExtension();
            $npwp->move($npwp_destination, $npwp_name);
            $pb->npwp = $npwp_name;
        }

        if($request->file('surat_nikah')){
            $sn = $request->file('surat_nikah');
            $sn_destination = 'doc/surat_nikah/';
            $sn_name = date('YmdHis') . "." . $sn->getClientOriginalExtension();
            $sn->move($sn_destination, $sn_name);
            $pb->surat_nikah = $sn_name;
        }

        if($request->file('surat_akta')){
            $sa = $request->file('surat_akta');
            $sa_destination = 'doc/surat_akta/';
            $sa_name = date('YmdHis') . "." . $sa->getClientOriginalExtension();
            $sa->move($sa_destination, $sa_name);
            $pb->surat_akta = $sa_name;
        }

        $pb->save();

        $al = Alamat::find($al->id);
        $al->status = "Tidak Tersedia";
        $al->save();

        return $pb;
    }

    public function deletePembelian(Request $request)
    {
        $pb = Pembelian::find($request->id);
        $al = Alamat::find($pb->alamat_id);
        $al->status = "tersedia";
        $al->save();
        $pb->deleted_at = Carbon::now();
        $pb->save();

        return $pb;
    }
}
