<?php

namespace App\Http\Controllers;

use App\Models\Alamat;
use App\Models\Daftarpengembalian;
use App\Models\Inventarisoperasional;
use App\Models\Pembelian;
use App\Models\Pengembalian;
use Carbon\Carbon;
use Illuminate\Http\Request;

class PengembalianController extends Controller
{
    public function getAll()
    {
        $peng = Pengembalian::where('deleted_at',null)->get();
        $data = array();
        foreach($peng as $pe){
            $pm = Pembelian::find($pe->pembelian_id);
            $sum = Daftarpengembalian::where('pengembalian_id',$pe->id)->sum('jumlah');
            array_push($data,[
                'id'=>$pe->id,
                'nama_pembeli'=>$pm->nama_pembeli,
                'lokasi_rumah'=>$pm->alamatpembelian->alamat,
                'total'=>$sum
            ]);
        }
        return $data;
    }

    public function addPengembalian(Request $request)
    {
        $peng = new Pengembalian();
        $pm = Pembelian::where('nama_pembeli', $request->nama_pembeli)->first();
        $peng->pembelian_id = $pm->id;
        $peng->keterangan = $request->keterangan;
        $peng->save();

        for($x=0;$x<count($request->daftarpengembalians);$x++){
            $dafpeng = new Daftarpengembalian();
            $dafpeng->pengembalian_id = $peng->id;
            $dafpeng->tanggal = $request->daftarpengembalians[$x]['tanggal'];
            $dafpeng->jumlah =  preg_replace("/[^0-9]/", "",$request->daftarpengembalians[$x]['jumlah']);
            $dafpeng->save();
        }

        if($this->checkTerpenuhi($peng->id)){
            $al = Alamat::find($pm->alamat_id);
            $al->status = 'tersedia';
            $al->save();
        }

        return $peng;
    }

    public function getPengembalian(Request $request)
    {
        $peng = Pengembalian::find($request->id);
        $peng->pembelian->alamatpembelian;
        $peng->daftarpengembalian;
        return $peng;
    }

    public function updatePengembalian(Request $request)
    {
        $peng = Pengembalian::find($request->id);
        $pm = Pembelian::where('nama_pembeli', $request->nama_pembeli)->first();
        $peng->pembelian_id = $pm->id;
        $peng->keterangan = $request->keterangan;
        $peng->save();

        $delete = Daftarpengembalian::where('pengembalian_id',$request->id)->delete();
        for($x=0;$x<count($request->daftarpengembalians);$x++){
            $dafpeng = new Daftarpengembalian();
            $dafpeng->pengembalian_id = $request->id;
            $dafpeng->tanggal = $request->daftarpengembalians[$x]['tanggal'];
            $dafpeng->jumlah =  preg_replace("/[^0-9]/", "",$request->daftarpengembalians[$x]['jumlah']);
            $dafpeng->save();
        }

        if($this->checkTerpenuhi($request->id)){
            $al = Alamat::find($pm->alamat_id);
            $al->status = 'tersedia';
            $al->save();
        }

        return $peng;
    }

    public function viewPengembalian($id)
    {
        $peng = Pengembalian::find($id);
        $peng->pembelian->alamatpembelian;
        $peng->daftarpengembalian;
        $sum = Daftarpengembalian::where('pengembalian_id',$id)->sum('jumlah');
        $dafpeng = Daftarpengembalian::where('pengembalian_id',$id)->get();

        $data = array();
        array_push($data,[
            'nama_pembeli'=>$peng->pembelian->nama_pembeli,
            'alamat'=>$peng->pembelian->alamatpembelian->alamat,
            'tanggal_akad'=>$peng->pembelian->tanggal_akad,
            'tanggal_pembayaran'=>$peng->pembelian->tanggal_pembayaran,
            'total_pembayaran'=>"Rp. ".number_format($peng->pembelian->harga,0, ',' , '.'),
            'total_pengembalian'=>"Rp. ".number_format($sum,0, ',' , '.'),
            'sisa_pengembalian'=>"Rp. ".number_format($peng->pembelian->harga - $sum,0, ',' , '.'),
            'keterangan'=>$peng->keterangan,
            'daftarpengembalian'=>$dafpeng
    ]);

        return $data[0];
    }

    public function deletePengembalian(Request $request)
    {
        $deleteDaftar = Daftarpengembalian::where('pengembalian_id', $request->id)->delete();
        $delete = Pengembalian::find($request->id)->delete();

        return $delete;
    }

    public function checkTerpenuhi($id){

        $peng = Pengembalian::find($id);
        $peng->pembelian->alamatpembelian;
        $peng->daftarpengembalian;
        $sum = Daftarpengembalian::where('pengembalian_id',$id)->sum('jumlah');

        if($peng->pembelian->harga >= $sum){
            return true;
        }else{
            return false;
        }
    }
}
