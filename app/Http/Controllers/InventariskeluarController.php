<?php

namespace App\Http\Controllers;

use App\Models\Alamat;
use App\Models\Inventariskeluar;
use App\Models\Inventarismasuk;
use App\Models\Tukang;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

class InventariskeluarController extends Controller
{
    public function getAll()
    {
        $im = Inventariskeluar::where('deleted_at',null)->get();
        $data = array();
        foreach($im as $i){
            array_push($data,[
                'id'=>$i->id,
                'tanggal'=>$i->tanggal,
                'nama_barang'=>$i->barang->nama_barang,
                'satuan'=>$i->barang->satuan,
                'alamat'=>$i->alamat->alamat,
                'user'=>$i->tukang->nama,
                'status'=>$i->status,
                'kuantitas'=>$i->kuantitas,
                'harga_satuan'=>$i->harga_satuan,
                'total'=>$i->total
            ]);
        }
        return $data;
    }

    public function addInventariskeluar(Request $request)
    {

        $ik = new Inventariskeluar();
        $ik->barang_id = $request->barang_id;

        $alamat = Alamat::where('alamat', $request->alamat)->first();
        $ik->alamat_id = $alamat->id;

        $user = Tukang::where('nama', $request->user)->first();
        $ik->user_id = $user->id;

        $ik->tanggal = $request->tanggal;
        $ik->jam = $request->jam;
        $ik->kuantitas = $request->kuantitas;
        $ik->harga_satuan = preg_replace("/[^0-9]/", "",$request->harga_satuan );
        $ik->status = $request->status;
        $ik->total = preg_replace("/[^0-9]/", "",$request->total );
        $ik->save();

        return $ik;
    }

    public function getInventariskeluar(Request $request)
    {
        $im = Inventariskeluar::find($request->id);
        $im->Barang;
        $im->Alamat;
        $im->Tukang;
        return $im;
    }

    public function updateInventariskeluar(Request $request)
    {
        $ik = Inventariskeluar::find($request->id);

        $ik->barang_id = $request->barang_id;

        $alamat = Alamat::where('alamat', $request->alamat)->first();
        $ik->alamat_id = $alamat->id;

        $user = Tukang::where('nama', $request->user)->first();
        $ik->user_id = $user->id;

        $ik->tanggal = $request->tanggal;
        $ik->jam = $request->jam;
        $ik->kuantitas = $request->kuantitas;
        $ik->harga_satuan = preg_replace("/[^0-9]/", "",$request->harga_satuan );
        $ik->status = $request->status;
        $ik->total = preg_replace("/[^0-9]/", "",$request->total );
        $ik->save();

        return $ik;
    }

    public function deleteInventariskeluar(Request $request)
    {
        $im = Inventariskeluar::find($request->id);
        $im->deleted_at = Carbon::now();
        $im->save();

        return $im;
    }
}
