<html>
<head>
    {{--    <meta charset="UTF-8" />--}}
    {{--    <meta name="viewport" content="width=device-width, initial-scale=1.0" />--}}
    {{--    <link href="{{ asset('css/app.css') }}" rel="stylesheet">--}}
    <title>{{strtoupper("PENGGAJIAN - ".$penggajian->created_at->isoFormat('D MMMM Y')." - ".$penggajian->tukang->nama)}}</title>
</head>
<style>
    @font-face {
        font-family: 'Roboto';
        font-weight: normal;
        font-style: normal;
        font-variant: normal;
        src: url("http://fonts.googleapis.com/css?family=Roboto");
    }
    body{
        font-family: 'Roboto', sans-serif;
    }
    .with-border{
        border: 1px solid black;
    }
    .text-center{
        text-align: center;
    }
    .p-1{
        padding: 3px;
    }
    .font-small{
        font-size: 14px;
    }
</style>
<body class="font-small">
<h1 style="text-align: center; border-bottom: 1px solid black; padding-bottom: 5px;">KWITANSI PENGGAJIAN</h1><br>
<table style="width: 100%;">
    <tr>
        <td style="width: 15%;">Nama:</td>
        <td style="width: 35%; font-weight: bold;">{{$penggajian->tukang->nama}}</td>
        <td style="width: 15%;">Tanggal:</td>
        <td style="width: 35%; font-weight: bold;">{{$penggajian->created_at->isoFormat('D MMMM Y')}}</td>
    </tr>
    <tr>
        <td style="width: 15%;">Jabatan:</td>
        <td style="width: 35%; font-weight: bold;">{{$penggajian->tukang->jenis_jasa}}</td>
        <td style="width: 15%;">No Kwitansi:</td>
        <td style="width: 35%; font-weight: bold;">GAJ{{$penggajian->created_at->format('ymdhis')}}</td>
    </tr>
</table>
<table class="with-border" style="width: 100%; margin-top: 25px; border-collapse: collapse">
    <tr>
        <td class="with-border text-center" style="padding: 5px; width: 10%">No</td>
        <td class="with-border" style="padding: 5px; width: 35%; text-align: center;">Keterangan</td>
        <td class="with-border" style="padding: 5px; width: 55%; text-align: center;">Jumlah</td>
    </tr>
    <tr>
        <td class="with-border text-center p-1">1</td>
        <td class="with-border p-1">Gaji - {{$penggajian->created_at->isoFormat('MMMM Y')}}</td>
        <td class="with-border p-1">{{"Rp. ".number_format($penggajian->tukang->biaya_jasa,0, ',' , '.')}}</td>
    </tr>
    <tr>
        <td class="with-border text-center p-1" colspan="2">Terbilang</td>
        <td class="with-border p-1">{{ucwords((new Riskihajar\Terbilang\Terbilang)->make($penggajian->tukang->biaya_jasa, ' Rupiah'))}}</td>
    </tr>
</table>
<table style="width: 100%; margin-top: 50px;">
    <tr>
        <td class="text-center" style="width: 50%"></td>
        <td class="text-center" style="width: 50%; padding-top: 25px; padding-bottom: 50px;">Bekasi, {{\Carbon\Carbon::now()->isoFormat('D MMMM Y')}}</td>
    </tr>
    <tr>
        <td class="text-center" style="width: 50%">Tukang</td>
        <td class="text-center" style="width: 50%">Accounting</td>
    </tr>
    <tr>
        <td style="padding-top: 100px;" class="text-center" style="width: 50%">____________________</td>
        <td style="padding-top: 100px;" class="text-center" style="width: 50%">____________________</td>
    </tr>
</table>
</body>
</html>
