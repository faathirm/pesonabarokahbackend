<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pengembalian extends Model
{
    use HasFactory;

    public function daftarpengembalian()
    {
        return $this->hasMany(Daftarpengembalian::class, 'pengembalian_id', 'id');
    }

    public function pembelian()
    {
        return $this->hasOne(Pembelian::class, 'id', 'pembelian_id');
    }

}
