<?php

namespace App\Http\Controllers;

use App\Models\Gajikaryawan;
use App\Models\Keluhan;
use Carbon\Carbon;
use Illuminate\Http\Request;

class GajikaryawanController extends Controller
{
    public function getAll()
    {
        $data = array();
        $gaji = Gajikaryawan::where('deleted_at',null)->with('karyawan')->get();
        foreach($gaji as $ga){
            array_push($data,[
                'id'=>$ga->id,
                'tanggal'=>$ga->tanggal,
                'nama_karyawan'=>$ga->karyawan->nama,
                'gaji_karyawan'=>$ga->karyawan->gaji
            ]);
        }
        return $data;
    }

    public function addGajiKaryawan(Request $request)
    {
        $gaji = new Gajikaryawan();
        $gaji->karyawan_id = $request->id;
        $gaji->tanggal = $request->tanggal;
        $gaji->save();

        return $gaji;
    }

    public function getDetailGajiKaryawan($id)
    {
        $gaji = Gajikaryawan::find($id);
        $gaji->karyawan;
        return $gaji;
    }

    public function updateGajiKaryawan(Request $request)
    {
        $gaji = Gajikaryawan::find($request->id);
        $gaji->karyawan_id = $request->karyawan_id;
        $gaji->tanggal = $request->tanggal;
        $gaji->save();

        return $gaji;
    }

    public function deleteGajiKaryawan(Request $request)
    {
        $gaji = Gajikaryawan::find($request->id);
        $gaji->deleted_at = Carbon::now();
        $gaji->save();

        return $gaji;
    }
}
