<?php

namespace App\Http\Controllers;

use App\Models\Barang;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Models\Inventarismasuk;

class InventarismasukController extends Controller
{
    public function getAll()
    {
        $im = Inventarismasuk::where('deleted_at',null)->get();
        $data = array();
        foreach($im as $i){
            array_push($data,[
                'id'=>$i->id,
                'supplier'=>$i->supplier,
                'tanggal'=>$i->tanggal,
                'nama_barang'=>$i->barang->nama_barang,
                'satuan'=>$i->barang->satuan,
                'kuantitas'=>$i->kuantitas,
                'harga_satuan'=>$i->harga_satuan,
                'total'=>$i->total,
                'file'=>$i->file
            ]);
        }
        return $data;
    }

    public function addInventarismasuk(Request $request)
    {

        $im = new Inventarismasuk();
        $im->barang_id = $request->barang_id;
        $im->tipe = "MATERIAL";
        $im->supplier = $request->supplier;
        $im->tanggal = $request->tanggal;
        $im->harga_satuan = preg_replace("/[^0-9]/", "",$request->harga_satuan);
        $im->kuantitas = $request->kuantitas;
        $im->total = preg_replace("/[^0-9]/", "",$request->total);

        if($request->file('file')){
            $sa = $request->file('file');
            $sa_destination = 'doc/inventaris/';
            $sa_name = date('YmdHis') . "." . $sa->getClientOriginalExtension();
            $sa->move($sa_destination, $sa_name);
            $im->file = $sa_name;
        }

        $im->save();

        return $im;
    }

    public function getInventarismasuk(Request $request)
    {
        $im = Inventarismasuk::find($request->id);
        $im->Barang;
        return $im;
    }

    public function updateInventarismasuk(Request $request)
    {
        $im = Inventarismasuk::find($request->id);

        $im->barang_id = $request->barang_id;
        $im->supplier = $request->supplier;
        $im->tipe = "MATERIAL";
        $im->tanggal = $request->tanggal;
        $im->harga_satuan = preg_replace("/[^0-9]/", "",$request->harga_satuan);
        $im->kuantitas = $request->kuantitas;
        $im->total = preg_replace("/[^0-9]/", "",$request->total);

        if($request->file('file')){
            $sa = $request->file('file');
            $sa_destination = 'doc/inventaris/';
            $sa_name = date('YmdHis') . "." . $sa->getClientOriginalExtension();
            $sa->move($sa_destination, $sa_name);
            $im->file = $sa_name;
        }

        $im->save();

        return $im;
    }

    public function deleteInventarismasuk(Request $request)
    {
        $im = Inventarismasuk::find($request->id);
        $im->deleted_at = Carbon::now();
        $im->save();

        return $im;
    }
}
