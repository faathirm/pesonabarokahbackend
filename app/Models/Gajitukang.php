<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Gajitukang extends Model
{
    use HasFactory;

    public function alamat()
    {
        return $this->hasOne(Alamat::class, 'id', 'alamat_id');
    }

    public function tukang()
    {
        return $this->hasOne(Tukang::class, 'id', 'tukang_id');
    }

}
