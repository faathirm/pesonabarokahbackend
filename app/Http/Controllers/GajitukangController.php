<?php

namespace App\Http\Controllers;

use App\Models\Alamat;
use App\Models\Gajitukang;
use App\Models\Tukang;
use Carbon\Carbon;
use Illuminate\Http\Request;

class GajitukangController extends Controller
{
    public function getAllHarian()
    {
        return Gajitukang::where('deleted_at',null)->where('jenis','harian')->with('alamat')->with('tukang')->get();
    }

    public function getAllBorongan()
    {
        return Gajitukang::where('deleted_at',null)->where('jenis','borongan')->with('alamat')->with('tukang')->get();
    }

    public function addGajiTukangHarian(Request $request)
    {
        //GET ID
        $tukang = Tukang::where('nama',$request->tukang)->first();
        $alamat = Alamat::where('alamat',$request->alamat)->first();

        $gaji = new Gajitukang();
        $gaji->tukang_id = $tukang->id;
        $gaji->alamat_id = $alamat->id;
        $gaji->jenis = $request->jenis;
        $gaji->jasa = $tukang->biaya_jasa;
        $gaji->total_pengerjaan = $request->total_hari_kerja;
        $gaji->kasbon = preg_replace("/[^0-9]/", "",$request->jumlah_kasbon);
        $gaji->keterangan = $request->keterangan;
        $gaji->total = preg_replace("/[^0-9]/", "",$request->total);
        $gaji->save();

        return $gaji;
    }

    public function getDetailGajiTukang($id)
    {
        return Gajitukang::where('id',$id)->with('alamat')->with('tukang')->first();
    }

    public function updateGajiTukang(Request $request)
    {
        //GET ID
        $tukang = Tukang::where('nama',$request->tukang)->first();
        $alamat = Alamat::where('alamat',$request->alamat)->first();

        $gaji = Gajitukang::find($request->id);
        $gaji->tukang_id = $tukang->id;
        $gaji->alamat_id = $alamat->id;
        $gaji->jenis = $request->jenis;
        $gaji->jasa = $tukang->biaya_jasa;
        $gaji->total_pengerjaan = $request->total_hari_kerja;
        $gaji->kasbon = preg_replace("/[^0-9]/", "",$request->jumlah_kasbon);
        $gaji->keterangan = $request->keterangan;
        $gaji->total = preg_replace("/[^0-9]/", "",$request->total);
        $gaji->save();

        return $gaji;
    }

    public function deletePenggajian(Request $request)
    {
        $gaji = Gajitukang::find($request->id);
        $gaji->deleted_at = Carbon::now();
        $gaji->save();

        return $gaji;
    }
}
