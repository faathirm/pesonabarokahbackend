<?php

namespace App\Http\Controllers;

use App\Models\Barang;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BarangController extends Controller
{
    public function getAll()
    {
        $barangs = Barang::where('tipe','MATERIAL')->where('deleted_at',null)->get();
        $data = array();
        foreach($barangs as $barang){
            $barangMasuk = \App\Models\Inventarismasuk::where('barang_id',$barang->id)->sum("kuantitas");
            $barangKeluar = \App\Models\Inventariskeluar::where('barang_id',$barang->id)->sum("kuantitas");
            array_push($data,[
                'id'=>$barang->id,
                'kode_barang'=>$barang->kode_barang,
                'nama_barang'=>$barang->nama_barang,
                'satuan'=>$barang->satuan,
                'harga_satuan'=>$barang->harga_satuan,
                'jumlah'=>$barangMasuk-$barangKeluar
            ]);
        }
        return $data;
    }

    public function addBarang(Request $request)
    {
        $kodebarang = substr(strtoupper($request->nama_barang), 0, 2).substr(strtoupper($request->satuan), 0, 2).substr(preg_replace("/[^0-9]/", "",$request->harga_satuan),0, 2).rand(1,99);

        $barang = new Barang();
        $barang->kode_barang = $kodebarang;
        $barang->nama_barang = strtoupper($request->nama_barang);
        $barang->satuan = strtoupper($request->satuan);
        $barang->harga_satuan = preg_replace("/[^0-9]/", "",$request->harga_satuan);
        $barang->tipe = "MATERIAL";
        $barang->save();

        return $barang;
    }

    public function getBarang(Request $request)
    {
        $barangs = Barang::where('id',$request->id)->where('tipe','MATERIAL')->first();
        return $barangs;
    }

    public function updateBarang(Request $request)
    {
        $barang = Barang::find($request->id);
        $barang->nama_barang = strtoupper($request->nama_barang);
        $barang->satuan = strtoupper($request->satuan);
        $barang->harga_satuan = preg_replace("/[^0-9]/", "",$request->harga_satuan);
        $barang->save();

        return $barang;
    }

    public function deleteBarang(Request $request)
    {
        $barang = Barang::find($request->id);
        $barang->deleted_at = Carbon::now();
        $barang->save();

        return $barang;
    }
}
